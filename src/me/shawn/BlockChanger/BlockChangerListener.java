package me.shawn.BlockChanger;

import org.bukkit.event.Listener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockDamageEvent;

public class BlockChangerListener implements Listener
{
	public BlockChanger bc;

	public BlockChangerListener(BlockChanger myBC)
	{
		bc = myBC;
	}

	@EventHandler
	public void blockDamage(BlockDamageEvent event)
	{
		if(bc.enabled(event.getPlayer()))
		{
			event.getBlock().setTypeId(bc.material);
		}
	}
}