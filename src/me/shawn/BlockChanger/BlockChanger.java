package me.shawn.BlockChanger;

import java.util.ArrayList;
import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class BlockChanger extends JavaPlugin
{
	private static final Logger log = Logger.getLogger("Minecraft");
	public final ArrayList<Player> BlockChangerUsers = new ArrayList<Player>();
	public int material = 35;
	
	@Override
	public void onEnable()
	{
		getServer().getPluginManager().registerEvents(new BlockChangerListener(this), this);
		log.info("[BlockChanger] enabled successfully");
	}
	
	@Override
	public void onDisable()
	{
		log.info("[BlockChanger] disabled");
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if(commandLabel.equalsIgnoreCase("blockchanger"))
		{
			if(args.length > 0)
			{
				material = Integer.parseInt(args[0]);
			}
			
			toggleBlockChanger(sender);
		}
		
		return true;
	}
	
	private void toggleBlockChanger(CommandSender sender)
	{
		if(!(sender instanceof Player)) return;
		Player p = (Player)sender;
		
		if(!enabled(p))
		{
			BlockChangerUsers.add(p);
			p.sendMessage(ChatColor.BLUE + "BlockChanger has been enabled");
		}
		else
		{
			BlockChangerUsers.remove(p);
			p.sendMessage(ChatColor.RED + "BlockChanger has been disabled");
		}
	}
	
	public boolean enabled(Player p)
	{
		return BlockChangerUsers.contains(p);
	}
}
