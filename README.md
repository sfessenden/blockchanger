01/04/2013 BLOCKCHANGER

This plugin changes block material when a block is damaged.

Usage:
  /blockchanger - enable/disable toggle.
